#include "unity_fixture.h"

static void RunAllTests(void)
{
    // RTOS Mock Tests
    RUN_TEST_GROUP(RTOSthreads);
    RUN_TEST_GROUP(RTOStimers);
    RUN_TEST_GROUP(RTOSevents);

    // Lab 3
    RUN_TEST_GROUP(RGB_LED);
    RUN_TEST_GROUP(Button);
    RUN_TEST_GROUP(Encoder);
    RUN_TEST_GROUP(Potentiometer);
    RUN_TEST_GROUP(Light);
    RUN_TEST_GROUP(RTOSDimmer);
}

int main(int argc, const char * argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}