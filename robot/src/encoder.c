#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "encoder.h"

static void _encoder_enable_interrupts(void);
static void _encoder_disable_interrupts(void);

static uint8_t _is_init = 0;
static int32_t _count = 0;

void encoder_init(void)
{
    // Set default count
    _count = 0; 

    if (!_is_init)  // Guards against multiple initialisations
    {
        /* TODO:    Configure and initialise PC0 and PC1 with
                    - External interrupts on the rising and falling edges
                    - Pullup resistors enabled                  */
        /* TODO: Enable GPIOC clock */
        /* TODO: Initialise GPIOC */
        /* TODO: Set the priorities of the EXTI0 and EXTI1 interrupts to
                    - Preempt: 0x02, 
                    - Sub: 0x00 */
        /* TODO: Enable the EXTI0 and EXTI1 IRQs using the NVIC */
        _is_init = 1;
    }
}

void encoder_edge_A_isr(void)
{
    // TODO: Implement A edge logic to increment or decrement _count
}

void encoder_edge_B_isr(void)
{
    // TODO: Implement B edge logic to increment or decrement _count
}

void encoder_set_count(int32_t count)
{
    // Atomically set _count
    _encoder_disable_interrupts();
    _count = count;
    _encoder_enable_interrupts();
}

int32_t encoder_get_count(void)
{
    // Atomically read _count
    _encoder_disable_interrupts();
    uint32_t count = _count;
    _encoder_enable_interrupts();
    return count;
}

int32_t encoder_pop_count(void)
{
    // Atomically read and reset _count
    _encoder_disable_interrupts();
    int32_t count = _count;
    _count = 0;
    _encoder_enable_interrupts();
    return count;
}

void _encoder_enable_interrupts(void)
{
    /* TODO: Enable the EXTI0 and EXTI1 IRQs using the NVIC */
}

void _encoder_disable_interrupts(void)
{
    /* TODO: Disable the EXTI0 and EXTI1 IRQs using the NVIC */
}

void EXTI0_IRQHandler(void)
{
    /* TODO: Call the encoder edge A isr */
    /* TODO: Call the HAL GPIO EXTI IRQ Handler and specify the GPIO pin */
}

void EXTI1_IRQHandler(void)
{
    /* TODO: Call the encoder edge B isr */
    /* TODO: Call the HAL GPIO EXTI IRQ Handler and specify the GPIO pin */
}